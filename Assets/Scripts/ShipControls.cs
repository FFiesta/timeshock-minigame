﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipControls : MonoBehaviour {

	float speed;
	float turnSpeed;
	float motion;

	Vector3 eulerAngleVelocity;

	float motionEnd;

	public float analogStickThreshold = .5f;

	Rigidbody rb;
	
	void Awake() {
		rb = GetComponent<Rigidbody>();
	}

	void Update () {
		turnSpeed = GetComponent<Ship>().turnSpeed;
		speed = GetComponent<Mover>().speed;

		
	}

	void FixedUpdate()
	{
		if (Input.GetAxisRaw("Horizontal") <= -analogStickThreshold) {
			motion = -turnSpeed;
		}
		else if (Input.GetAxisRaw("Horizontal") >= analogStickThreshold) {
			motion = turnSpeed;
		} else {
			/* I must make it so that it falls to the 
			middle-bottom of the screen, with inertia, gravity-wise.
			I'm lacking math and physics knowledge here. */
			if (rb.rotation.z > 0)
				motion -= turnSpeed * (turnSpeed / 64) * Time.deltaTime ;
			if (rb.rotation.z < 0)
				motion += turnSpeed * (turnSpeed / 64) * Time.deltaTime;

		}
		eulerAngleVelocity = new Vector3(0,0,motion);
		rb.velocity = new Vector3(0,0,speed);
		
		Quaternion deltaRotation = Quaternion.Euler(eulerAngleVelocity * Time.deltaTime);
		rb.MoveRotation(rb.rotation * deltaRotation);
	}
}
