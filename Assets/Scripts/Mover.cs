﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

	public float speed = 8;

	Rigidbody rb;

	void Awake () {
		rb = GetComponent<Rigidbody>();
	}
	void Update () {
		rb.velocity = new Vector3(0,0,speed);
	}
}
